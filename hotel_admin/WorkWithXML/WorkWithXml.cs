﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DataInOut
{
    public class WorkWithXml<T>
    {           

        public void DoSerialize(List<T> list, string classname)
        {
                       
                XmlSerializer formatter = new XmlSerializer(typeof(List<T>));

                using (FileStream fs = new FileStream($"{classname}"+".xml", FileMode.OpenOrCreate))
                {
                    formatter.Serialize(fs, list);
                    fs.Close();
                }
           
        }

        public List<T> DoDeserialize(List<T> list, string classname)
        {
                     
                XmlSerializer formatter = new XmlSerializer(typeof(List<T>));

                using (FileStream fs = new FileStream($"{classname}" +".xml", FileMode.OpenOrCreate))
                {
                    list = (List<T>)formatter.Deserialize(fs);
                }

            return list;
          
        }
    }
}
