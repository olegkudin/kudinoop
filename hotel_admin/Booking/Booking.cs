﻿using DataInOut;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookingClass
{
    public class Booking
    {
        public int Number { get; set; }
        public DateTime DateIn { get; set; }
        public DateTime DateOut { get; set; }
        
        List<Booking> bookings = new List<Booking>();
        WorkWithXml<Booking> ser = new WorkWithXml<Booking>();

        public Booking()
        { }

        public void RecordingToBookingList(int number_room, DateTime datein, DateTime dateout)
        {
            Booking booking = new Booking();
            if (File.Exists("Booking.xml") == true)
            {
                bookings = ser.DoDeserialize(bookings, "Booking");
            }
            booking.Number = number_room;
            booking.DateIn = datein;
            booking.DateOut = dateout;
            bookings.Add(booking);
            ser.DoSerialize(bookings, "Booking");
                     
        }
    }
}
