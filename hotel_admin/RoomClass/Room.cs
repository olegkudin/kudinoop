﻿using BookingClass;
using DataInOut;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace RoomClass
{
    [Serializable]
    public class Room
    {
        public int Number { get; set; }
        public EStatus Status { get; set; }
        List<Room> rooms = new List<Room>();
        WorkWithXml<Room> ser = new WorkWithXml<Room>();
        public Room()
        { }

        public string ChangeRoomStatus(string mode, int number)
        {
            
            try
            {                
                if (File.Exists("Room.xml") != true)
                {
                    throw new FileNotFoundException("File room.xml doesn`t found... Back to menu");
                }
            }
            
            catch (FileNotFoundException e)
            {
                Console.WriteLine(e.Message);
                System.Threading.Thread.Sleep(3000);
                return string.Format(e.Message);              
            }

            XDocument xdocRoom = XDocument.Load("Room.xml");
            XElement root = xdocRoom.Element("ArrayOfRoom");

            foreach (XElement xe in root.Elements("Room").ToList())
            {
                switch (mode)
                {
                    case ("zanyato"):
                        if (xe.Element("Number").Value == Convert.ToString(number))
                        {
                            xe.Element("Status").Value = Convert.ToString(EStatus.Zanyato);
                        }
                        break;

                    case ("svobodno"):
                        if (xe.Element("Number").Value == Convert.ToString(number))
                        {
                            xe.Element("Status").Value = Convert.ToString(EStatus.Svobodno);
                        }
                        break;

                    case ("bronirovat"):
                        if (xe.Element("Number").Value == Convert.ToString(number))
                        {
                            xe.Element("Status").Value = Convert.ToString(EStatus.Zabronirovano);
                        }
                        break;
                }
            }
            xdocRoom.Save("Room.xml");
            return "";
        }

        public string ShowFreeRooms()
        {            
            string result="";
            try
            {
                if (File.Exists("Room.xml") != true)
                {
                    throw new FileNotFoundException("File Room.xml doesn`t found...");
                }
                XDocument xdocRoom = XDocument.Load("Room.xml");
                var roomstatus = from xe in xdocRoom.Element("ArrayOfRoom").Elements("Room")
                                 where xe.Element("Status").Value == Convert.ToString(EStatus.Svobodno) || xe.Element("Status").Value == Convert.ToString(EStatus.Zabronirovano)
                                 select new Room
                                 {
                                     Number = Convert.ToInt32(xe.Element("Number").Value),
                                     Status = (EStatus)Enum.Parse(typeof(EStatus), xe.Element("Status").Value)
                                 };

                foreach (var room in roomstatus)
                {
                    result += string.Format($"\nNumber of room: {room.Number} \nStatus: {room.Status} \n\n");
                }
            }
            catch (FileNotFoundException e)
            {
                Console.WriteLine(e.Message);
                System.Threading.Thread.Sleep(3000);
                return string.Format(e.Message);
            }
            return result;
        }

        public string CurrentRoomStatus()
        {          
            string result = "";
            try
            {
                if (File.Exists("Room.xml") != true)
                {
                    throw new FileNotFoundException("File Room.xml doesn`t found... Back to menu");
                }
                rooms=ser.DoDeserialize(rooms,"Room");

                foreach (var room in rooms)
                {
                    result += string.Format($"Number of room: {room.Number} \nStatus: {room.Status} \n\n");
                }
            }
            catch (FileNotFoundException e)
            {
                Console.WriteLine(e.Message);
                System.Threading.Thread.Sleep(3000);
                return string.Format(e.Message);
            }
            return result;
        }

        public string isRoomNotAvailable(DateTime datein, DateTime dateout)
        {
            string result = "";
            try
            {
                if (File.Exists("Booking.xml") != true || File.Exists("Room.xml") != true)
                {
                    throw new FileNotFoundException("Some files doesn`t found... Back to menu");
                }
            }
            catch (FileNotFoundException e)
            {                
                System.Threading.Thread.Sleep(3000);
                return e.Message;
            }

            XDocument xdocBooking = XDocument.Load("Booking.xml");
            XDocument xdocRoom = XDocument.Load("Room.xml");


            var datestatus = from xe in xdocBooking.Element("ArrayOfBooking").Elements("Booking")
                             from xe2 in xdocRoom.Element("ArrayOfRoom").Elements("Room")
                             where (Convert.ToDateTime(xe.Element("DateIn").Value) >= datein && Convert.ToDateTime(xe.Element("DateOut").Value) <= dateout && xe.Element("Number").Value == xe2.Element("Number").Value) || (Convert.ToDateTime(xe.Element("DateIn").Value) <= datein && Convert.ToDateTime(xe.Element("DateOut").Value) >= datein && xe.Element("Number").Value == xe2.Element("Number").Value) || (Convert.ToDateTime(xe.Element("DateIn").Value) <= dateout && Convert.ToDateTime(xe.Element("DateOut").Value) >= dateout && xe.Element("Number").Value == xe2.Element("Number").Value)
                             select new
                             {
                                 Number = Convert.ToInt32(xe2.Element("Number").Value),
                                 Status = (EStatus)Enum.Parse(typeof(EStatus), xe2.Element("Status").Value)
                             };

            if (datestatus != null)
            {
                foreach (var room in datestatus)
                {
                    result += string.Format($"Number of room: {room.Number} \nStatus: {room.Status} \n\n");
                }
            }
            else return "All rooms is free";
            return result;
        }

        public string RoomDateStatus(DateTime date)
        {
            var result = "";            
            List<int> otherRooms = new List<int>();
            try
            {
                if (File.Exists("Room.xml") != true || File.Exists("Booking.xml") != true)
                {
                    throw new FileNotFoundException("Some files doesn`t found...");
                }
                
                XDocument xdocRoom = XDocument.Load("Room.xml");
                XDocument xdocBooking = XDocument.Load("Booking.xml");
                var datestatus = from xe in xdocBooking.Element("ArrayOfBooking").Elements("Booking")
                                 from xe2 in xdocRoom.Element("ArrayOfRoom").Elements("Room")
                                 where Convert.ToDateTime(xe.Element("DateIn").Value) <= date && Convert.ToDateTime(xe.Element("DateOut").Value) >= date && xe2.Element("Number").Value == xe.Element("Number").Value
                                 select new Room
                                 {
                                     Number = Convert.ToInt32(xe2.Element("Number").Value),
                                     Status = (EStatus)Enum.Parse(typeof(EStatus), xe2.Element("Status").Value)
                                 };

                foreach (var room in datestatus)
                {
                    result += string.Format($"\nNumber of room: {room.Number} \nStatus: {room.Status} \n\n");
                    otherRooms.Add(room.Number);

                }

                datestatus = from xe2 in xdocRoom.Element("ArrayOfRoom").Elements("Room")
                             where otherRooms.Contains(Convert.ToInt32(xe2.Element("Number").Value)) != true
                             select new Room
                             {
                                 Number = Convert.ToInt32(xe2.Element("Number").Value),
                                 Status = EStatus.Svobodno
                             };

                foreach (var room in datestatus)
                {
                    result += string.Format($"\nNumber of room: {room.Number} \nStatus: {room.Status} \n\n");
                }
              
            }
            catch (FileNotFoundException e)
            {
                return e.Message;                
            }
            return result;           
        }
    }
}
