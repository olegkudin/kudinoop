﻿using BookingClass;
using DataInOut;
using RoomClass;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace ClientClass
{
    [Serializable]
    public class Client
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Patronymic { get; set; }
        public string Pass_srs { get; set; }
        public int Number_room { get; set; }
        public DateTime DateIn { get; set; }
        public DateTime DateOut { get; set; }
        List<Client> clients = new List<Client>();
        WorkWithXml<Client> ser = new WorkWithXml<Client>();
        Booking booking = new Booking();
        Room room = new Room();

        public Client()       
        {
           
        }

        public void Registarion(string name, string surname, string patronymic, string pass_srs, int number_room, DateTime datein, DateTime dateout)
        {
            Client currClient = new Client();
            
            if (File.Exists("Client.xml") == true)
            {
                clients = ser.DoDeserialize(clients, "Client");
            }
            currClient.Name = name;
            currClient.Surname = surname;
            currClient.Patronymic = patronymic;
            currClient.Pass_srs = pass_srs;            
            currClient.Number_room = number_room;
            currClient.DateIn = datein;
            currClient.DateOut = dateout;
            clients.Add(currClient);
            ser.DoSerialize(clients, "Client");           
        }

        public string ClientOut(string surname, string passSrs)
        {
            try
            {
                if (File.Exists("Client.xml") != true || File.Exists("Room.xml") != true || File.Exists("Booking.xml") != true)
                {
                    throw new FileNotFoundException("Some files doesn`t found... Back to menu");
                }
            }
            catch (FileNotFoundException e)
            {
                 return e.Message;                             
            }
            XDocument xdocBooking = XDocument.Load("Booking.xml");
            XDocument xdocClient = XDocument.Load("Client.xml");
            XDocument xdocRoom = XDocument.Load("Room.xml");
            
            string tmpNumber = "";
            var checkResult = 0;
            XElement rootClient = xdocClient.Element("ArrayOfClient");

            foreach (XElement xe in rootClient.Elements("Client").ToList())
            {
                if (xe.Element("Surname").Value == surname && xe.Element("Pass_srs").Value == passSrs)
                {                    
                    {
                        xe.Remove();
                        tmpNumber = xe.Element("Number_room").Value;
                        room.ChangeRoomStatus("svobodno", Convert.ToInt32(tmpNumber));

                        XElement rootBooking = xdocBooking.Element("ArrayOfBooking");
                        foreach (XElement xe2 in rootBooking.Elements("Booking").ToList())
                        {
                            if (xe2.Element("Number").Value == tmpNumber)
                            {
                                xe2.Remove();
                                xdocBooking.Save("Booking.xml");
                            }
                        }

                        checkResult++;
                    }                    
                }

            }
            if (checkResult == 0)
            {
                return "Client is not found";                
            }
            xdocClient.Save("Client.xml");
            return "Client is evicted";
        }
    }

    
}
