﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractOperations
{
    public abstract class AbstractCasualOperation
    {
        private string _result;

        public abstract void Calculating(double firstOperand, double secondOperand);

        public string GetResult()
        {
            
            return _result;
        }
        
    }
}
