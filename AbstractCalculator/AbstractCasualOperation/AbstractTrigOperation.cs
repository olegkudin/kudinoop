﻿namespace AbstractOperations
{
    public abstract class AbstractTrigOperation
    {
        private string _result;

        public abstract void Calculating(double Operand);

        public string GetResult()
        {           
            return _result;
        }
    }
}
