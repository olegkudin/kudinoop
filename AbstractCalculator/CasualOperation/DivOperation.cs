﻿using AbstractOperations;
using System;

namespace AbstractOperations
{
    public class DivOperation: AbstractCasualOperation
    {
        private string _result;

        public override void Calculating(double firstOperand, double secondOperand)
        {
            if (secondOperand == 0)
            {
                throw new DivideByZeroException();
            }
            _result = Convert.ToString(firstOperand / secondOperand);
        }

        public string GetResult(double firstOperand, double secondOperand)
        {
            Calculating(firstOperand, secondOperand);    
            return String.Format("{0}/{1}={2}", firstOperand, secondOperand, _result);
        }
    
    }
}
