﻿using AbstractOperations;
using System;

namespace CasualOperation
{
    public class MultiOperation: AbstractCasualOperation
    {
        private string _result;

        public override void Calculating(double firstOperand, double secondOperand)
        {
            _result = Convert.ToString(firstOperand * secondOperand);
        }

        public string GetResult(double firstOperand, double secondOperand)
        {
            Calculating(firstOperand, secondOperand);
            return String.Format("{0}*{1}={2}", firstOperand, secondOperand, _result);
        }
    
    }
}
