﻿using AbstractManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrigOperation;
using CasualOperation;
using AbstractOperations;

namespace MainManager
{
    public class Manager: AbstractMainManager
    {
        private readonly IDictionary<string, ETypeOperation> typesOperation = new Dictionary<string, ETypeOperation>();

        private readonly SubOperation Sub = new SubOperation();
        private readonly SumOperation Sum = new SumOperation();
        private readonly DivOperation Div = new DivOperation();
        private readonly MultiOperation Multi = new MultiOperation();

        private readonly Sin Sin = new Sin();
        private readonly Cos Cos = new Cos();
        private readonly Tan Tan = new Tan();
        private readonly Ctan Ctan = new Ctan();


        public Manager()
        {
            typesOperation.Add("1", ETypeOperation.plus);
            typesOperation.Add("2", ETypeOperation.minus);
            typesOperation.Add("3", ETypeOperation.div);
            typesOperation.Add("4", ETypeOperation.mul);
            typesOperation.Add("5", ETypeOperation.sin);
            typesOperation.Add("6", ETypeOperation.cos);
            typesOperation.Add("7", ETypeOperation.tan);
            typesOperation.Add("8", ETypeOperation.ctan);
        }

        private ETypeOperation GetOperation(string operationName)
        {
            var content = typesOperation[operationName];
            return content;
        }

        

        public override void MainManager()
        {
            Console.WindowHeight = 25;
            Console.BufferHeight = 25;
            Console.WindowWidth = 60;
            Console.BufferWidth = 60;
            Console.SetCursorPosition(19, 0);
            Console.WriteLine("<<New Calculator>>\n\n");
            Console.WriteLine("List of functions: \n1. Sum(+)\n2. Subtraction(-)\n3. Division(/)\n4. Multiplication(*)\n5. sin(x)\n6. cos(x)\n7. tg(x)\n8. ctg(x)");
            Console.WriteLine("\nChoose number of function:");

            double SecondOperand = 0;
            try
            {
                string Operation = Console.ReadLine();
                var typeOperation = GetOperation(Operation);
                Console.WriteLine("\nType first operand: ");
                var FirstOperand = double.Parse(Console.ReadLine());
                if (typeOperation == ETypeOperation.plus || typeOperation == ETypeOperation.minus || typeOperation == ETypeOperation.div || typeOperation == ETypeOperation.mul)
                {
                    Console.WriteLine("Type second operand: ");
                    SecondOperand = double.Parse(Console.ReadLine());
                }

                switch (typeOperation)
                {
                    //Сума
                    case (ETypeOperation.plus):
                        Console.WriteLine(Sum.GetResult(FirstOperand, SecondOperand));
                        break;

                    //Вычетание
                    case (ETypeOperation.minus):
                        Console.WriteLine(Sub.GetResult(FirstOperand, SecondOperand));
                        break;

                    //Деление
                    case (ETypeOperation.div):
                        Console.WriteLine(Div.GetResult(FirstOperand, SecondOperand));
                        break;

                    //Умножение
                    case (ETypeOperation.mul):
                        Console.WriteLine(Multi.GetResult(FirstOperand, SecondOperand));
                        break;

                    // Синус
                    case (ETypeOperation.sin):

                        Console.WriteLine(Sin.GetResult(FirstOperand));
                        break;

                    //Косинус
                    case (ETypeOperation.cos):
                        Console.WriteLine(Cos.GetResult(FirstOperand));
                        break;

                    //Тангенс
                    case (ETypeOperation.tan):
                        Console.WriteLine(Tan.GetResult(FirstOperand));
                        break;
                    //Котангенс
                    case (ETypeOperation.ctan):
                        Console.WriteLine(Ctan.GetResult(FirstOperand));
                        break;

                    default:
                        Console.WriteLine("\nCant find that operation");
                        break;
                }

            }
            catch (FormatException)
            {
                Console.WriteLine("Incorrect data");
            }
            catch (DivideByZeroException)
            {
                Console.WriteLine("Divide by zero");
            }
            catch (KeyNotFoundException)
            {
                Console.WriteLine("Invalid operation");
            }
            Console.WriteLine("\nDo you want to continue?[y/n]");
            var answer = Console.ReadKey().Key;
            if (answer == ConsoleKey.Y)
            {
                Console.Clear();
                MainManager();
            }
            else
            if (answer == ConsoleKey.N)
            {
                Environment.Exit(1);
            }
        }
        
    }
}
