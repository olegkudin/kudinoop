﻿using AbstractOperations;

namespace AbstractManager
{
    public abstract class AbstractMainManager
    {
        public abstract void MainManager();        
    }
}
