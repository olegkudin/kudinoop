﻿using MainManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbstractManager;

namespace AbstractCalculator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.BackgroundColor = ConsoleColor.White;
            Console.ForegroundColor = ConsoleColor.Black;
            var MainApp = new Manager();
            MainApp.MainManager();
            Console.ReadLine();
        }
    }
}
