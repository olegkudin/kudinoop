﻿using AbstractOperations;
using System;

namespace TrigOperation
{
    public class Tan: AbstractTrigOperation
    {
        private string _result;

        public override void Calculating(double Operand)
        {
            _result = Convert.ToString(Math.Tan(Operand));
        }

        public string GetResult(double Operand)
        {
            Calculating(Operand);
            return String.Format("Tan({0})={1}", Operand, _result);
        }
    
    }
}
