﻿using AbstractOperations;
using System;

namespace TrigOperation
{
    public class Sin: AbstractTrigOperation
    {
        private string _result;

        public override void Calculating(double Operand)
        {
            _result = Convert.ToString(Math.Sin(Operand));
        }

        public string GetResult(double Operand)
        {
            Calculating(Operand);
            return String.Format("sin({0})={1}", Operand, _result);
        }
    }
}
