﻿using AbstractOperations;
using System;


namespace TrigOperation
{
    public class Cos: AbstractTrigOperation
    {
        private string _result;

        public override void Calculating(double Operand)
        {
            _result = Convert.ToString(Math.Cos(Operand));
        }

        public string GetResult(double Operand)
        {
            Calculating(Operand);
            return String.Format("cos({0})={1}", Operand, _result);
        }
    
    }
}
