﻿using AbstractOperations;
using System;

namespace TrigOperation
{
    public class Ctan: AbstractTrigOperation
    {
        private string _result;

        public override void Calculating(double Operand)
        {
            _result = Convert.ToString(1/Math.Tan(Operand));
        }

        public string GetResult(double Operand)
        {
            Calculating(Operand);
            return String.Format("Ctan({0})={1}", Operand, _result);
        }
    
    }
}
