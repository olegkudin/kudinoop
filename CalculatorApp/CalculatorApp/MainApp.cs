﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculatorApp
{
    class MainApp
    {
        static void Main(string[] args)
        {
            Manager manager = new Manager();
            manager.Calculating();
            while (true)
            {
                manager.Restart();
            }
            Console.ReadLine();
        }
    }
}
