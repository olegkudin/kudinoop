﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MainCalculate;


namespace CalculatorApp
{
    class Manager
    {
        private readonly Calculate _MainCalculate;

        public Manager()
        {
            _MainCalculate = new Calculate();
        }

        public void Calculating()
        {
            Console.WriteLine("Please, choose function[+ - * /]: ");
            var func = Console.ReadLine();
            Console.WriteLine("Type first number for the function");
            double number1 = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Type second number for the function");
            double number2 = Convert.ToDouble(Console.ReadLine());
            switch (func)
            {
                case "+":
                    var result = _MainCalculate.GetFunction(func, number1, number2);
                    Console.WriteLine(result);
                    break;

                case "-":
                    result = _MainCalculate.GetFunction(func, number1, number2);
                    Console.WriteLine(result);
                    break;

                case "*":
                    result = _MainCalculate.GetFunction(func, number1, number2);
                    Console.WriteLine(result);
                    break;

                case "/":
                    try
                    {
                        if (number2 == 0)
                            throw new DivideByZeroException();
                        else
                        {
                            result = _MainCalculate.GetFunction(func, number1, number2);
                            Console.WriteLine(result);
                        }
                    }
                    catch (DivideByZeroException)
                    {
                        Console.WriteLine("You can divide by zero");

                    }
                    finally
                    { }
                    break;

                default:
                    Console.WriteLine("Cant do this function");
                    break;


            }

        }

        public void Restart()
        {
            Console.WriteLine("Would you want to start again[y/n]: ");
            var answer = Console.ReadLine();
            switch (answer)
            {
                case "y":
                    Console.Clear();
                    Calculating();
                    break;

                case "n":
                    Environment.Exit(0);
                    break;

                default:
                    Console.WriteLine("Type \"y\" or \"n\" next time, bye");
                    break;
            }
            
        }
    }
}
