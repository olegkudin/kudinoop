﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace MainCalculate
{
    public class Calculate
    {
        private double _number1;
        private double _number2;
        private double _result;

        public double number1 { get; set; }
        public double number2 { get; set; }
        public double result { get; set; }

        private string Plus(double number1, double number2)
        {
            result = number1 + number2;
            string answer = string.Format("Res: {0}+{1}={2}", number1, number2, result);
            return answer;
        }

        private string Minus(double number1, double number2)
        {
            result = number1 - number2;
            string answer = string.Format("Res: {0}-{1}={2}", number1, number2, result);
            return answer;
        }

        private string Multi(double number1, double number2)
        {
            result = number1 * number2;
            string answer = string.Format("Res: {0}*{1}={2}", number1, number2, result);
            return answer;
        }

        private string Div(double number1, double number2)
        {
            result = number1 / number2;
            string answer = string.Format("Res: {0}/{1}={2}", number1, number2, result);
            return answer;
        }

        public string GetFunction(string func, double number1, double number2)
        {
            switch (func)
            {
                case "+":
                   return Plus(number1, number2);
                    break;
                case "-":
                  return  Minus(number1, number2);
                    break;
                case "/":
                 return   Div(number1, number2);
                    break;
                case "*":
                  return  Multi(number1, number2);
                    break;
                default:
                    return "";
                    break;
            }
           
        }
    }
}
