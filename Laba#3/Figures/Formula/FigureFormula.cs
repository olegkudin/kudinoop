﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Formula
{
    public class FigureFormula
    {
        public const double Pi = 3.14;

        private double _area;
        private double _perimeter;

        public double Area { set; get; }
        public double Perimeter { set; get; }

        //квадрат
        public string Square(double SideA)
        {
            Area = SideA * SideA;
            string result = string.Format("Square area: {0}\n", Area);
            return result;
        }

        public string SquarePerimeter(double SideA)
        {
            Perimeter = 4 * SideA;
            string result = string.Format("Square perimeter: {0}\n", Perimeter);
            return result;

        }

        //прямоугольник
        public string Square(double SideA, double SideB)
        {
            Area = SideA * SideB;
            string result = string.Format("Rectangle area: {0}\n", Area);
            return result;
        }

        public string SquarePerimeter(double SideA, double SideB)
        {
            Perimeter = 2 * (SideA+SideB);
            string result = string.Format("Rectangle perimeter: {0}\n", Perimeter);
            return result;

        }

        //параллелограмм
        public string Square(double SideA, double SideB, double Angle)
        {
            Area = SideA * SideB * Math.Sin(Pi*Angle/180);
            string result = string.Format("Parallelogramm area: {0}\n", Area);
            return result;
        }

        public string ParallelogrammPerimeter(double SideA, double SideB)
        {
            Perimeter = 2 * (SideA + SideB);
            string result = string.Format("Parallelogramm perimeter: {0}\n", Perimeter);
            return result;

        }

        //круг
        public string CircleArea(double Radius)
        {
            Area = Pi * Radius * Radius;
            string result = string.Format("Circle area: {0}\n", Area);
            return result;
        }

        public string CirclePerimeter(double Radius)
        {
            Perimeter = 2 * Radius * Pi;
            string result = string.Format("Circle perimeter: {0}\n", Perimeter);
            return result;

        }

        //треугольник
        public string Triangle(double SideA, double SideB, double Angle)
        {
            Area = 0.5 * SideA * SideB * Math.Sin(Pi * Angle / 180);
            string result = string.Format("Triangle area: {0}\n", Area);
            return result;
        }

        public string TrianglePerimeter(double SideA, double SideB, double SideC)
        {
            Perimeter = SideA + SideB + SideC;
            string result = string.Format("Triangle perimeter: {0}\n", Perimeter);
            return result;

        }

        //прямоугольный треугольник
        public string RightTriangle(double SideA, double SideB)
        {
            Area = 0.5 * SideA * SideB;
            string result = string.Format("Right triangle area: {0}\n", Area);
            return result;
        }


    }
}
