﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Formula;

namespace Figures
{
   public class Manager
    {
        private readonly FigureFormula _Formula;

        public Manager()
        {
            _Formula = new FigureFormula();
        }

        public void GetFormula()
        {
            double sideA, sideB, sideC, Angle, Radius;
            
            Console.WriteLine("Choose the figure: \nSquare - 1\nRectangle - 2\nParallelogramm - 3\nCircle - 4\nTriangle - 5\nRight triangle - 6");
            var Answer = Console.ReadLine();
            switch (Answer)
            {
                case "1":
                    Console.WriteLine("Enter side A length:");
                    sideA = Convert.ToDouble(Console.ReadLine());
                    Console.WriteLine(_Formula.Square(sideA));
                    Console.WriteLine(_Formula.SquarePerimeter(sideA));
                    break;

                case "2":
                    Console.WriteLine("Enter side A length:");
                    sideA = Convert.ToDouble(Console.ReadLine());
                    Console.WriteLine("Enter side B length:");
                    sideB = Convert.ToDouble(Console.ReadLine());
                    Console.WriteLine(_Formula.Square(sideA, sideB));
                    Console.WriteLine(_Formula.SquarePerimeter(sideA, sideB));
                    break;

                case "3":
                    Console.WriteLine("Enter side A length:");
                    sideA = Convert.ToDouble(Console.ReadLine());
                    Console.WriteLine("Enter side B length:");
                    sideB = Convert.ToDouble(Console.ReadLine());                
                    Console.WriteLine("Enter angle:");
                    Angle = Convert.ToDouble(Console.ReadLine());
                    Console.WriteLine(_Formula.Square(sideA, sideB, Angle));
                    Console.WriteLine(_Formula.ParallelogrammPerimeter(sideA, sideB));
                    break;

                case "4":
                    Console.WriteLine("Enter circle radius:");
                    Radius = Convert.ToDouble(Console.ReadLine());
                    Console.WriteLine(_Formula.CircleArea(Radius));
                    Console.WriteLine(_Formula.CirclePerimeter(Radius));
                    break;

                case "5":
                    Console.WriteLine("Enter side A length:");
                    sideA = Convert.ToDouble(Console.ReadLine());
                    Console.WriteLine("Enter side B length:");
                    sideB = Convert.ToDouble(Console.ReadLine());
                    Console.WriteLine("Enter side C length:");
                    sideC = Convert.ToDouble(Console.ReadLine());
                    Console.WriteLine("Enter angle:");
                    Angle = Convert.ToDouble(Console.ReadLine());
                    Console.WriteLine(_Formula.Triangle(sideA, sideB, Angle));
                    Console.WriteLine(_Formula.TrianglePerimeter(sideA, sideB, sideC));
                    break;

                case "6":
                    Console.WriteLine("Enter side A length:");
                    sideA = Convert.ToDouble(Console.ReadLine());
                    Console.WriteLine("Enter side B length:");
                    sideB = Convert.ToDouble(Console.ReadLine());
                    Console.WriteLine("Enter side C length:");
                    sideC = Convert.ToDouble(Console.ReadLine());
                    Console.WriteLine(_Formula.RightTriangle(sideA, sideB));
                    Console.WriteLine(_Formula.TrianglePerimeter(sideA, sideB, sideC));
                    break;

                default:
                    Console.WriteLine("Choose other figure");
                    break;


            }
        }
    }
}
