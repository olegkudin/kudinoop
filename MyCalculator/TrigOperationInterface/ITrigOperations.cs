﻿namespace TrigOperationInterface
{
    public interface ITrigOperations
    {
        double TrigCalculating(double Operand);
    }
}
