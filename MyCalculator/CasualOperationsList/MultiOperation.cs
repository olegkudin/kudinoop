﻿using CasualOperationInterface;

namespace CasualOperationsList
{
    public class MultiOperation: ICasualOperations
    {
        public double Calculating(double firstOperand, double secondOperand)
        {
            return firstOperand * secondOperand;
        }
    }
}
