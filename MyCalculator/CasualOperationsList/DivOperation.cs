﻿using CasualOperationInterface;
using System;

namespace CasualOperationsList
{
    public class DivOperation: ICasualOperations
    {
        public double Calculating(double firstOperand, double secondOperand)
        {
            if (secondOperand == 0)
            {
                throw new DivideByZeroException();
            }
            return firstOperand / secondOperand;
        }
    }
}
