﻿using CasualOperationInterface;

namespace CasualOperationsList
{
    public class SumOperation: ICasualOperations
    {
        public double Calculating(double firstOperand, double secondOperand)
        {
            return firstOperand + secondOperand;
        }
    }
}
