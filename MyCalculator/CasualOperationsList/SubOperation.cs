﻿using CasualOperationInterface;
using System;

namespace CasualOperationsList
{
    public class SubOperation: ICasualOperations
    {
        public double Calculating(double firstOperand, double secondOperand)
        {
           
            return firstOperand - secondOperand;
        }
    }
}
