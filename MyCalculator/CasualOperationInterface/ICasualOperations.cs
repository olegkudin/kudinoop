﻿namespace CasualOperationInterface
{
    public interface ICasualOperations
    {
        double Calculating(double firstOperand, double secondOperand);
    }
}
