﻿using CasualOperationInterface;
using CasualOperationsList;
using MainCalculatingInterface;
using System;
using System.Collections.Generic;
using TrigOperationInterface;
using TrigOperationList;

namespace MainCalculate
{
    public class Calculating: IMainCalculating
    {
        private readonly IDictionary<string, ETypeOperation> typesOperation=new Dictionary<string, ETypeOperation>();

        public Calculating()
        {
            typesOperation.Add("1", ETypeOperation.plus);
            typesOperation.Add("2", ETypeOperation.minus);
            typesOperation.Add("3", ETypeOperation.div);
            typesOperation.Add("4", ETypeOperation.mul);
            typesOperation.Add("5", ETypeOperation.sin);
            typesOperation.Add("6", ETypeOperation.cos);
            typesOperation.Add("7", ETypeOperation.tan);
            typesOperation.Add("8", ETypeOperation.ctan);
        }
        public double GetResult(ICasualOperations Operation, double FirstOperand, double SecondOperand)
        {
            var result = Operation.Calculating(FirstOperand, SecondOperand);
            return result;
        }

        public double GetResult(ITrigOperations Operation, double Operand)
        {
            var result = Operation.TrigCalculating(Operand);
            return result;
        }

        private ETypeOperation GetOperation(string operationName)
        {                       
            var content = typesOperation[operationName];
            return content;
        }

        public void Manager()
        {
            Console.WindowHeight = 25;
            Console.BufferHeight = 25;
            Console.WindowWidth = 60;
            Console.BufferWidth = 60;
            Console.SetCursorPosition(19, 0);
            Console.WriteLine("<<New Calculator>>\n\n");
            Console.WriteLine("List of functions: \n1. Sum(+)\n2. Subtraction(-)\n3. Division(/)\n4. Multiplication(*)\n5. sin(x)\n6. cos(x)\n7. tg(x)\n8. ctg(x)");
            Console.WriteLine("\nChoose number of function:");
           
            double SecondOperand=0;
            try
            {
                string Operation = Console.ReadLine();
                var typeOperation = GetOperation(Operation);
                Console.WriteLine("\nType first operand: ");
                    var FirstOperand = double.Parse(Console.ReadLine());
                if (typeOperation == ETypeOperation.plus || typeOperation == ETypeOperation.minus || typeOperation == ETypeOperation.div || typeOperation == ETypeOperation.mul)
                {
                    Console.WriteLine("Type second operand: ");
                    SecondOperand = double.Parse(Console.ReadLine());
                }
                                             
                switch (typeOperation)
                {
                    //Сума
                    case (ETypeOperation.plus):
                        Console.WriteLine("\n{1}+{2} = {0}", GetResult(new SumOperation(), FirstOperand, SecondOperand), FirstOperand, SecondOperand);
                        break;

                    //Вычетание
                    case (ETypeOperation.minus):
                        Console.WriteLine("\n{1}-{2} = {0}", GetResult(new SubOperation(), FirstOperand, SecondOperand), FirstOperand, SecondOperand);
                        break;

                    //Деление
                    case (ETypeOperation.div):
                        Console.WriteLine("\n{1}/{2} = {0}", GetResult(new DivOperation(), FirstOperand, SecondOperand), FirstOperand, SecondOperand);
                        break;

                    //Умножение
                    case (ETypeOperation.mul):
                        Console.WriteLine("\n{1}*{2} = {0}", GetResult(new MultiOperation(), FirstOperand, SecondOperand), FirstOperand, SecondOperand);
                        break;

                    // Синус
                    case (ETypeOperation.sin):
                                                   
                            Console.WriteLine("\nsin({1}) = {0}", GetResult(new Sin(), FirstOperand), FirstOperand);                     
                        break;

                    //Косинус
                    case (ETypeOperation.cos):                                                
                            Console.WriteLine("\ncos({1}) = {0}", GetResult(new Cos(), FirstOperand), FirstOperand);                      
                        break;

                    //Тангенс
                    case (ETypeOperation.tan):                       
                            Console.WriteLine("\ntg({1}) = {0}", GetResult(new Tan(), FirstOperand), FirstOperand);                       
                        break;
                    //Котангенс
                    case (ETypeOperation.ctan):                                                  
                            Console.WriteLine("\nctg({1}) = {0}", GetResult(new Ctan(), FirstOperand), FirstOperand);
                        break;

                    default:
                        Console.WriteLine("\nCant find that operation");
                        break;
                }

            }
            catch (FormatException)
            {
                Console.WriteLine("Incorrect data");
            }
            catch (DivideByZeroException)
            {
                Console.WriteLine("Divide by zero");
            }
            catch (KeyNotFoundException)
            {
                Console.WriteLine("Invalid operation");
            }
            Console.WriteLine("\nDo you want to continue?[y/n]");
            var answer = Console.ReadKey().Key;
            if (answer == ConsoleKey.Y)
            {
                Console.Clear();
                Manager();
            }
            else
            if (answer == ConsoleKey.N)
            {
                Environment.Exit(1);
            }
        }
    }
}

