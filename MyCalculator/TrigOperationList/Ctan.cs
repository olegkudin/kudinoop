﻿using System;
using TrigOperationInterface;

namespace TrigOperationList
{
    public class Ctan: ITrigOperations
    {
        public double TrigCalculating(double Operand)
        {
            return 1/Math.Tan(Operand);
        }
    }
}
