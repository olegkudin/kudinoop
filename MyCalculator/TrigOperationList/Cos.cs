﻿using System;
using TrigOperationInterface;

namespace TrigOperationList
{
    public class Cos: ITrigOperations
    {
        public double TrigCalculating(double Operand)
        {
            return Math.Cos(Operand);
        }
    }
}
