﻿using System;
using TrigOperationInterface;

namespace TrigOperationList
{
    public class Sin: ITrigOperations
    {
        public double TrigCalculating(double Operand)
        {
            return Math.Sin(Operand);
        }
    }
}
