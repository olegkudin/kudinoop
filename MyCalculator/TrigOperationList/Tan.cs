﻿using System;
using TrigOperationInterface;

namespace TrigOperationList
{
    public class Tan: ITrigOperations
    {
        public double TrigCalculating(double Operand)
        {
            return Math.Tan(Operand);
        }
    }
}
