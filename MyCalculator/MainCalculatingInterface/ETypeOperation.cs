﻿namespace MainCalculatingInterface
{
    public enum ETypeOperation
    {
        plus,
        minus,
        div,
        mul,
        sin,
        cos,
        tan,
        ctan
    }
}
