﻿using CasualOperationInterface;
using TrigOperationInterface;

namespace MainCalculatingInterface
{
    public interface IMainCalculating
    {    
            double GetResult(ICasualOperations Operation, double FirstOperand, double SecondOperand);

            double GetResult(ITrigOperations Operation, double Operand);

            void Manager();       
    }
}
