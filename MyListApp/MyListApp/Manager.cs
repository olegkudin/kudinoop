﻿using MyList;
using System;


namespace MyListApp
{
    class Manager<T>
    {
        public Manager(MyList<T> list)
        {
            list.addingValue += addingHandler;
            list.AfterAdd += afterAddhandler;
            list.removingValue += removingHandler;
            list.AfterRemove += afterRemovehandler;
        }
        public void addingHandler(object sender, ChangeValue<T> e)
        {
            if(e.GetType()==typeof(int))
            {
                ChangeValue<int> validate;
                object buffer =(object)e;
                if((validate=buffer as ChangeValue<int>)!=null && validate.Value%2 == 0)
                {
                    validate.Value++;
                }
            }
        }
        public void removingHandler(object sender, ChangeValue<T> e)
        {
            if (e.Value.GetType() == typeof(int))
            {
                ChangeValue<int> validate;
                object buffer = (object)e;
                if ((validate = buffer as ChangeValue<int>) != null && validate.Value%2 != 0)
                {
                    validate.Value--;
                }
            }
        }
        public void afterAddhandler(object sender, ChangeValue<T> e)
        {
            Show(sender);
            if(e.Value.GetType() == typeof(int))
             Console.WriteLine($"Added value:{e.Value}");
        }
        public void afterRemovehandler(object sender, ChangeValue<T> e)
        {
            Show(sender);
            if (e.Value.GetType() == typeof(int))
                Console.WriteLine($"Removed value:{e.Value}");
        }
        public void Show(object sender)
        {
            var outList = (MyList<T>)sender;
            Console.WriteLine(outList.ToString());
        }
    }
}
