﻿using System;

namespace MyList
{
    public class ChangeValue<T>:EventArgs
    {
        private  T _value;
        public T Value
        {
            get { return _value; }
            set { _value = value; }
        }
        public ChangeValue(T value)
        {
            _value = value;
        }
    }
}
