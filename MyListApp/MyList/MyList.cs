﻿using System;
using System.Collections.Generic;

namespace MyList
{
    public class MyList<T>
    {
        public event EventHandler<ChangeValue<T>> addingValue;
        public event EventHandler<ChangeValue<T>> AfterAdd;
        public event EventHandler<ChangeValue<T>> removingValue;
        public event EventHandler<ChangeValue<T>> AfterRemove;

        private List<T> list = new List<T>();

        public void AddHandler(ChangeValue<T> e)
        {
            var myEvent = addingValue;
            if (myEvent != null)
                myEvent(this,e);
            list.Add(e.Value);
            myEvent = AfterAdd;
            if (myEvent != null)
                myEvent(this,e);
        }
        public void RemoveHandler(ChangeValue<T> e)
        {
            var myEvent = removingValue;
            if (myEvent != null)
                myEvent(this, e);
            list.Remove(e.Value);
            myEvent = AfterRemove;
            if (myEvent != null)
                myEvent(this, e);
        }
        public void Add(T value)
        {
            var e = new ChangeValue<T>(value);
            AddHandler(e);
        }
        public void Remove(T value)
        {
            var e = new ChangeValue<T>(value);
            RemoveHandler(e);
        }
        public void Sort()
        {
            list.Sort();
        }
        public override string ToString()
        {
            string result="";
            foreach (var el in list)
                result+=el.ToString()+" ";
            return result; 
        }
    }
}
