﻿namespace CasualOperationInterface
{
    public interface ICasualOperations
    {
        void Calculating(double firstOperand, double secondOperand);
        DCasualOutput CasualOutput { get; set;}
    }
}
