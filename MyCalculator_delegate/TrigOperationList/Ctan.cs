﻿using System;
using TrigOperationInterface;

namespace TrigOperationList
{
    public class Ctan: ITrigOperations
    {
        public DTrigOutput TrigOutput { get; set; }
        public void TrigCalculating(double Operand)
        {
            var result = 1/Math.Tan(Operand);
            TrigOutput(Operand, "tan", result);
        }
    }
}
