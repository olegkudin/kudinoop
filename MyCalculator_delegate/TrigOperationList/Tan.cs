﻿using System;
using TrigOperationInterface;

namespace TrigOperationList
{
    public class Tan : ITrigOperations
    {
        public DTrigOutput TrigOutput { get; set; }
        public void TrigCalculating(double Operand)
        {
            var result = Math.Tan(Operand);
            TrigOutput(Operand, "tan", result);
        }
    }
}