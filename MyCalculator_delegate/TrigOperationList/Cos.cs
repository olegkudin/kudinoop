﻿using System;
using TrigOperationInterface;

namespace TrigOperationList
{
    public class Cos: ITrigOperations
    {
        public DTrigOutput TrigOutput { get; set; }
        public void TrigCalculating(double Operand)
        {           
            var result = Math.Cos(Operand);
            TrigOutput(Operand, "cos", result);
        }
    }
}
