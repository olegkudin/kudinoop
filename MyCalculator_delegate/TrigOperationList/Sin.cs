﻿using System;
using TrigOperationInterface;

namespace TrigOperationList
{
    public class Sin: ITrigOperations
    {
        public DTrigOutput TrigOutput { get; set; }
        public void TrigCalculating(double Operand)
        {
            var result = Math.Sin(Operand);
            TrigOutput(Operand, "sin", result);
        }
    }
}
