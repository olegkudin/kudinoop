﻿using CasualOperationInterface;
using CasualOperationsList;
using MainCalculatingInterface;
using System;
using System.Collections.Generic;
using TrigOperationInterface;
using TrigOperationList;

namespace MainCalculate
{
    public class Calculating
    {
        private readonly IDictionary<string, ETypeOperation> typesOperation=new Dictionary<string, ETypeOperation>();
        

        public Calculating()
        {
            typesOperation.Add("1", ETypeOperation.plus);
            typesOperation.Add("2", ETypeOperation.minus);
            typesOperation.Add("3", ETypeOperation.div);
            typesOperation.Add("4", ETypeOperation.mul);
            typesOperation.Add("5", ETypeOperation.sin);
            typesOperation.Add("6", ETypeOperation.cos);
            typesOperation.Add("7", ETypeOperation.tan);
            typesOperation.Add("8", ETypeOperation.ctan);
            
            
        }


        public void OutputValue(double firstOperand, string typeOperator, double secondOperand, double result)
        {            
            Console.WriteLine($"{firstOperand} {typeOperator} {secondOperand} = {result}");
        }

        public void OutputValue(double Operand, string typeOperator, double result)
        {
            Console.WriteLine($"{typeOperator}({Operand})  = {result}");
        }


        public void GetResult(ICasualOperations Operation, double FirstOperand, double SecondOperand)
        {
            Operation.CasualOutput += OutputValue;
            Operation.Calculating(FirstOperand, SecondOperand);           
        }

        public void GetResult(ITrigOperations Operation, double Operand)
        {
            Operation.TrigOutput += OutputValue;
            Operation.TrigCalculating(Operand);

        }

        private ETypeOperation GetOperation(string operationName)
        {                       
            var content = typesOperation[operationName];
            return content;
        }

        public void Manager()
        {
            Console.WindowHeight = 25;
            Console.BufferHeight = 25;
            Console.WindowWidth = 60;
            Console.BufferWidth = 60;
            Console.SetCursorPosition(19, 0);
            Console.WriteLine("<<New Calculator>>\n\n");
            Console.WriteLine("List of functions: \n1. Sum(+)\n2. Subtraction(-)\n3. Division(/)\n4. Multiplication(*)\n5. sin(x)\n6. cos(x)\n7. tg(x)\n8. ctg(x)");
            Console.WriteLine("\nChoose number of function:");           
            
            double SecondOperand=0;
            try
            {
                var numberOperation = Console.ReadLine();
                var typeOperation = GetOperation(numberOperation);
                Console.WriteLine("\nType first operand: ");
                    var FirstOperand = double.Parse(Console.ReadLine());
                if (typeOperation == ETypeOperation.plus || typeOperation == ETypeOperation.minus || typeOperation == ETypeOperation.div || typeOperation == ETypeOperation.mul)
                {
                    Console.WriteLine("Type second operand: ");
                    SecondOperand = double.Parse(Console.ReadLine());
                }
                                             
                switch (typeOperation)
                {
                    //Сума
                    case (ETypeOperation.plus):
                        {                            
                            GetResult(new SumOperation(), FirstOperand, SecondOperand);
                            break;
                        }

                    //Вычетание
                    case (ETypeOperation.minus):
                        GetResult(new SubOperation(), FirstOperand, SecondOperand);
                        break;

                    //Деление
                    case (ETypeOperation.div):
                        GetResult(new DivOperation(), FirstOperand, SecondOperand);
                        break;

                    //Умножение
                    case (ETypeOperation.mul):
                        GetResult(new MultiOperation(), FirstOperand, SecondOperand);
                        break;

                    // Синус
                    case (ETypeOperation.sin):

                        GetResult(new Sin(), FirstOperand);
                        break;

                    //Косинус
                    case (ETypeOperation.cos):
                        GetResult(new Cos(), FirstOperand);
                        break;

                    //Тангенс
                    case (ETypeOperation.tan):
                        GetResult(new Tan(), FirstOperand);
                        break;
                    //Котангенс
                    case (ETypeOperation.ctan):
                        GetResult(new Ctan(), FirstOperand);
                        break;
                        
                    default:
                        Console.WriteLine("\nCant find that operation");
                        break;
                }

            }
            catch (FormatException)
            {
                Console.WriteLine("Incorrect data");
            }
            catch (DivideByZeroException)
            {
                Console.WriteLine("Divide by zero");
            }
            catch (KeyNotFoundException)
            {
                Console.WriteLine("Invalid operation");
            }
            Console.WriteLine("\nDo you want to continue?[y/n]");
            var answer = Console.ReadKey().Key;
            if (answer == ConsoleKey.Y)
            {
                Console.Clear();
                Manager();
            }
            else
            if (answer == ConsoleKey.N)
            {
                Environment.Exit(1);
            }
        }
    }
}

