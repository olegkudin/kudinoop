﻿
namespace MainCalculatingInterface
{
    public interface IMainCalculating
    {    
            void GetResult(double FirstOperand, double SecondOperand);

            void GetResult(double Operand);

            void Manager();       
    }
}
