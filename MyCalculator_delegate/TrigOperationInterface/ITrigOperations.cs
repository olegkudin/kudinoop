﻿namespace TrigOperationInterface
{
    public interface ITrigOperations
    {
        void TrigCalculating(double Operand);
        DTrigOutput TrigOutput { get; set; }
    }
}
