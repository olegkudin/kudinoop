﻿using CasualOperationInterface;
using MainCalculatingInterface;

namespace CasualOperationsList
{
    public class MultiOperation: ICasualOperations
    {
        public DCasualOutput CasualOutput { get; set; }
        public void Calculating(double firstOperand, double secondOperand)
        {
            var result = firstOperand * secondOperand;
            CasualOutput(firstOperand, "*", secondOperand, result);
        }
    }
}
