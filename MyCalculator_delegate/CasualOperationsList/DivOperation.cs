﻿using CasualOperationInterface;
using MainCalculatingInterface;
using System;

namespace CasualOperationsList
{
    public class DivOperation: ICasualOperations
    {
        public DCasualOutput CasualOutput { get; set; }
        public void Calculating(double firstOperand, double secondOperand)
        {
            if (secondOperand == 0)
            {
                throw new DivideByZeroException();
            }
            var result = firstOperand / secondOperand;
            CasualOutput(firstOperand, "/", secondOperand, result);
        }
    }
}
