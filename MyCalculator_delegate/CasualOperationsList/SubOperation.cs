﻿using CasualOperationInterface;
using MainCalculatingInterface;
using System;

namespace CasualOperationsList
{
    public class SubOperation: ICasualOperations
    {
        public DCasualOutput CasualOutput { get; set; }
        public void Calculating(double firstOperand, double secondOperand)
        {
            var result = firstOperand - secondOperand;
            CasualOutput(firstOperand, "-", secondOperand, result);
        }
    }
}
