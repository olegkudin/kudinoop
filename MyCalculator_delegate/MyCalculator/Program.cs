﻿using CasualOperationsList;
using MainCalculate;
using System;

namespace MyCalculator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.BackgroundColor = ConsoleColor.White;
            Console.ForegroundColor = ConsoleColor.Black;
            var MainApp = new Calculating();
            MainApp.Manager();
            Console.ReadLine();
        }
    }
}
