﻿using BookingClass;
using ClientClass;
using RoomClass;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace MainManager
{
    public class Manager
    {
        Client client = new Client();
        Room room = new Room();
        Booking booking = new Booking();

        
        XDocument xdocBooking;
        XDocument xdocClient;
        XDocument xdocRoom;

        public void Menu()
        {
            Console.Clear();
            Console.WriteLine("Menu");
            Console.WriteLine("Choose function:");
            Console.WriteLine("1. Registration");
            Console.WriteLine("2. Show current rooms status");
            Console.WriteLine("3. Show rooms status for date");
            Console.WriteLine("4. Room reservation");
            Console.WriteLine("5. Client out");           
            var answer = Console.ReadLine();

            switch (answer)
            {
                case ("1"):
                    DoRegistration();
                    BackToMenu();
                    break;               

                case ("2"):
                    Console.WriteLine(room.CurrentRoomStatus()); 
                    BackToMenu();
                    break;

                 case ("3"):
                     RoomDateStatus();
                     BackToMenu();
                     break;

                case ("4"):
                    RoomReservation();
                    BackToMenu();
                    break;

                case ("5"):
                    ClientOut();
                    BackToMenu();
                    break;               
            }
        }

        public void DoRegistration()
        {
            try
            {                           
                Console.Clear();
                Console.WriteLine("Registraion:");
                Console.Write("Name: ");
                var Name = Console.ReadLine();
                Console.Write("Surname: ");
                var Surname = Console.ReadLine();
                Console.Write("Patronymic: ");
                var Patronymic = Console.ReadLine();
                Console.Write("Passport series: ");
                var Pass_srs = Console.ReadLine();
                var freeRooms = room.ShowFreeRooms();
                Console.WriteLine(freeRooms);
                Console.Write("Room number: ");
                var Number_room = Convert.ToInt32(Console.ReadLine());                
                if (freeRooms.IndexOf(Convert.ToString(Number_room) + " ") == -1)
                {
                    throw new InvalidDataException($"\nThis room is not available... Back to start");
                }
                Console.Write("Date come in: ");
                var DateIn = DateTime.ParseExact(Console.ReadLine(), "dd.MM.yyyy", CultureInfo.InvariantCulture);
                Console.Write("Date come out: ");
                var DateOut = DateTime.ParseExact(Console.ReadLine(), "dd.MM.yyyy", CultureInfo.InvariantCulture);
                if (DateIn > DateOut)
                {
                    throw new InvalidDataException($"\nDate come in can`t be longer than date come out... Back to start");                   
                }
                var roomavailable = isRoomNotAvailable(DateIn, DateOut);
                if (roomavailable.IndexOf(Convert.ToString(Number_room)) >= 0)
                {
                    throw new InvalidDataException($"\nRoom is not available for this date... Back to start");
                }
                    Console.WriteLine("Save it? [y/n]");
                var answer = Console.ReadLine();
                if (answer == "y")
                {
                    client.Registarion(Name, Surname, Patronymic, Pass_srs, Number_room, DateIn, DateOut);
                    room.ChangeRoomStatus("zanyato", Number_room);
                    booking.RecordingToBookingList(Number_room, DateIn, DateOut);
                }
                else if (answer == "n")
                    DoRegistration();
                else Menu();

            }
            catch (FormatException)
            {
                Console.WriteLine($"\nIncorrect data... Back to start");
                System.Threading.Thread.Sleep(2000);
                DoRegistration();
            }
            

            catch (InvalidDataException e)
            {
                Console.WriteLine(e.Message);
                System.Threading.Thread.Sleep(3000);
                DoRegistration();
            }                                   
        }

        public void BackToMenu()
        {
            Console.WriteLine("Back to menu? [y/n]");
            var answer = Console.ReadLine();
            if (answer == "y")
                Menu();
            else if (answer == "n")
                Process.GetCurrentProcess().Kill();
            else Menu();
        }

           public void ClientOut()
           {
               Console.Clear();
               try
               {
                   if (File.Exists("Client.xml") != true || File.Exists("Room.xml") != true || File.Exists("Booking.xml") != true)
                   {
                       throw new FileNotFoundException("Some files doesn`t found... Back to menu");
                   }
               }
               catch (FileNotFoundException e)
               {
                   Console.WriteLine(e.Message);
                   System.Threading.Thread.Sleep(3000);
                   Menu();
               }
               xdocClient = XDocument.Load("Client.xml");
               xdocRoom = XDocument.Load("Room.xml");
               xdocBooking = XDocument.Load("Booking.xml");
               Console.WriteLine("Type surname: ");
               var tmpSurname = Console.ReadLine();
               Console.WriteLine("Type passport series: ");
               var tmpPassSrs = Console.ReadLine();
               string tmpNumber="";
               var checkResult = 0;
               XElement rootClient = xdocClient.Element("ArrayOfClient");

               foreach (XElement xe in rootClient.Elements("Client").ToList())
               {
                   if (xe.Element("Surname").Value == tmpSurname && xe.Element("Pass_srs").Value == tmpPassSrs)
                   {
                       Console.WriteLine("Evict this client?[y/n]");
                       var answer = Console.ReadLine();
                       if (answer == "y")
                       {
                           xe.Remove();                        
                           tmpNumber = xe.Element("Number_room").Value;                        
                           room.ChangeRoomStatus("svobodno", Convert.ToInt32(tmpNumber));

                           XElement rootBooking = xdocBooking.Element("ArrayOfBooking");
                           foreach (XElement xe2 in rootBooking.Elements("Booking").ToList())
                           {
                               if (xe2.Element("Number").Value == tmpNumber)
                               {
                                   xe2.Remove();
                                   xdocBooking.Save("Booking.xml");
                               }
                           }

                           checkResult++;
                       }
                       else if (answer == "n")
                       {
                           ClientOut();
                       }
                       else BackToMenu();
                   }

               }
               if (checkResult == 0)
               {
                   Console.WriteLine("Client is not found");
                   BackToMenu();
               }
               xdocClient.Save("Client.xml");          
           }



           public void RoomReservation()
           {
            Console.Clear();
            try
            {
                Console.WriteLine("Reservation:");
                Console.WriteLine("Room number: ");
                var Number = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Date to come in: ");
                var DateIn = DateTime.ParseExact(Console.ReadLine(), "dd.MM.yyyy", CultureInfo.InvariantCulture);
                Console.WriteLine("Date to come out: ");
                var DateOut = DateTime.ParseExact(Console.ReadLine(), "dd.MM.yyyy", CultureInfo.InvariantCulture);
               
                if (DateIn > DateOut)
                {
                    throw new InvalidDataException($"\nDate come in can`t be longer than date come out... Back to start");
                }

                var roomavailable = isRoomNotAvailable(DateIn, DateOut);
                if (roomavailable.IndexOf(Convert.ToString(Number)) >= 0)
                {
                    throw new InvalidDataException($"\nRoom is not available for this date... Back to start");
                }

                Console.WriteLine("Save it? [y/n]");
                var answer = Console.ReadLine();
                if (answer == "y")
                {
                    booking.RecordingToBookingList(Number, DateIn, DateOut);
                    room.ChangeRoomStatus("bronirovat", Number);
                }
                else if (answer == "n")
                    RoomReservation();
                else Menu();

            }

            catch (FormatException)
            {
                Console.WriteLine($"\nIncorrect data... Back to start");
                System.Threading.Thread.Sleep(2000);
                RoomReservation();
            }

            catch (InvalidDataException e)
            {
                Console.WriteLine(e.Message);
                System.Threading.Thread.Sleep(3000);
                RoomReservation();
            }
        }
      

           public string isRoomNotAvailable(DateTime datein, DateTime dateout)
           {               
            string result = "";
               try
               {
                   if (File.Exists("Booking.xml") != true || File.Exists("Room.xml") != true)
                   {
                       throw new FileNotFoundException("Some files doesn`t found... Back to menu");
                   }
               }
               catch (FileNotFoundException e)
               {
                   Console.WriteLine(e.Message);
                   System.Threading.Thread.Sleep(3000);
                return "";
               }

               xdocBooking = XDocument.Load("Booking.xml");
               xdocRoom = XDocument.Load("Room.xml");
               
               
               var datestatus = from xe in xdocBooking.Element("ArrayOfBooking").Elements("Booking")
                                from xe2 in xdocRoom.Element("ArrayOfRoom").Elements("Room")
                                where (Convert.ToDateTime(xe.Element("DateIn").Value) >= datein && Convert.ToDateTime(xe.Element("DateOut").Value) <= dateout  && xe.Element("Number").Value == xe2.Element("Number").Value) || (Convert.ToDateTime(xe.Element("DateIn").Value) <= datein && Convert.ToDateTime(xe.Element("DateOut").Value) >= datein && xe.Element("Number").Value == xe2.Element("Number").Value) || (Convert.ToDateTime(xe.Element("DateIn").Value) <= dateout && Convert.ToDateTime(xe.Element("DateOut").Value) >= dateout && xe.Element("Number").Value == xe2.Element("Number").Value)
                                select new 
                                {
                                    Number = Convert.ToInt32(xe2.Element("Number").Value),
                                    Status = (EStatus)Enum.Parse(typeof(EStatus), xe2.Element("Status").Value)
                                };

               if (datestatus != null)
               {
                   foreach (var room in datestatus)
                   {
                       result += string.Format($"Number of room: {room.Number} \nStatus: {room.Status} \n\n");
                   }
               }
               else Console.WriteLine("All rooms is free");
            return result;
           }

        public void RoomDateStatus()
        {
            var result = "";
            Console.Clear();
            List<int> otherRooms = new List<int>();
            try
            {
                if (File.Exists("Room.xml") != true || File.Exists("Booking.xml") != true)
                {
                    throw new FileNotFoundException("Some files doesn`t found...");
                }
                Console.WriteLine("Type date:");
                var tmpDate = DateTime.ParseExact(Console.ReadLine(), "dd.MM.yyyy", CultureInfo.InvariantCulture);
                XDocument xdocRoom = XDocument.Load("Room.xml");
                XDocument xdocBooking = XDocument.Load("Booking.xml");
                var datestatus = from xe in xdocBooking.Element("ArrayOfBooking").Elements("Booking")
                                 from xe2 in xdocRoom.Element("ArrayOfRoom").Elements("Room")
                                 where Convert.ToDateTime(xe.Element("DateIn").Value) <= tmpDate && Convert.ToDateTime(xe.Element("DateOut").Value) >= tmpDate && xe2.Element("Number").Value == xe.Element("Number").Value
                                 select new Room
                                 {
                                     Number = Convert.ToInt32(xe2.Element("Number").Value),
                                     Status = (EStatus)Enum.Parse(typeof(EStatus), xe2.Element("Status").Value)
                                 };

                foreach (var room in datestatus)
                {
                   result+=string.Format($"\nNumber of room: {room.Number} \nStatus: {room.Status} \n\n");
                    otherRooms.Add(room.Number);
                    
                }

                datestatus =     from xe2 in xdocRoom.Element("ArrayOfRoom").Elements("Room")
                                 where otherRooms.Contains(Convert.ToInt32(xe2.Element("Number").Value))!=true
                                 select new Room
                                 {
                                     Number = Convert.ToInt32(xe2.Element("Number").Value),
                                     Status = EStatus.Svobodno/*(EStatus)Enum.Parse(typeof(EStatus), xe2.Element("Status").Value)*/
                                 };

                foreach (var room in datestatus)
                {
                    result += string.Format($"\nNumber of room: {room.Number} \nStatus: {room.Status} \n\n");
                }
                
                Console.WriteLine(result);
            }
            catch (FileNotFoundException e)
            {
                Console.WriteLine(e.Message);
                System.Threading.Thread.Sleep(3000);
                Menu();
            }
        }
    }
}
