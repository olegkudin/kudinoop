﻿using BookingClass;
using DataInOut;
using RoomClass;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientClass
{
    [Serializable]
    public class Client
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Patronymic { get; set; }
        public string Pass_srs { get; set; }
        public int Number_room { get; set; }
        public DateTime DateIn { get; set; }
        public DateTime DateOut { get; set; }
        List<Client> clients = new List<Client>();
        WorkWithXml<Client> ser = new WorkWithXml<Client>();
        Booking booking = new Booking();

        public Client()       
        {
           
        }

        public void Registarion(string name, string surname, string patronymic, string pass_srs, int number_room, DateTime datein, DateTime dateout)
        {
            Client currClient = new Client();
            Room room = new Room();
            if (File.Exists("Client.xml") == true)
            {
                clients = ser.DoDeserialize(clients, "Client");
            }
            currClient.Name = name;
            currClient.Surname = surname;
            currClient.Patronymic = patronymic;
            currClient.Pass_srs = pass_srs;            
            currClient.Number_room = number_room;
            currClient.DateIn = datein;
            currClient.DateOut = dateout;
            clients.Add(currClient);
            ser.DoSerialize(clients, "Client");           
        }
    }

    
}
