﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace student
{
    public class Students
    {
        private string _firstname;
        private string _lastname;
        private int _age;
        private string _group;
        private int _curse;

        public string FirstName
        { get { return _firstname; }
          set { _firstname = value; }
        } 
        public string LastName
        {
            get { return _lastname; }
            set { _lastname = value; }
        }
        public string Group
        {
            get { return _group; }
            set { _group = value; }
        }
        public int Curse
        {
            get { return _curse; }
            set {_curse = value; }
        }
        public int Age
        {
            get { return _age; }
            set {_age = value;}
        }

        public void SetStudent(string firstname, string lastname, int age, string group, int curse)
        {
            FirstName = firstname;
            LastName = lastname;
            Age = age;
            Group = group;
            Curse = curse;
        }

        public string GetStudent()
        {
            return string.Format("Last Name: {0}\nFirst Name: {1}\nAge: {2}\nCurse: {3}\nGpoup: {4}", LastName, FirstName, Age, Curse, Group);
        }
    }
}
