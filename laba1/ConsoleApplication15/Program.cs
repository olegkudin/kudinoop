﻿//20 мин
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using student;

namespace student
{
    class Program
    {
        static void Main(string[] args)
        {
         
            var Student = new Students();
            string answer;
            string firstname;
            string lastname;
            string group;
            int curse;
            int age;
            int k=0;
            Console.Clear();
            label1:
            Console.WriteLine("Mode: [in/out] ");
            answer = Console.ReadLine();
            switch (answer)
            {
                case "in":
                    Console.Write("Enter: \nFirst name: ");
                    firstname = Console.ReadLine();
                    Console.Write("Last name: ");
                    lastname = Console.ReadLine();

                    do
                    {
                        k = 0;
                        Console.Write("Age: ");
                        age = Convert.ToInt32(Console.ReadLine());
                        try
                        {
                            if (age < 1)
                                throw new Exception("Age cant be less than 1");
                            else k++;
                        }

                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }

                    }
                    while (k!=1);

                    do
                    {
                        k = 0;
                        Console.Write("Curse: ");
                        curse = Convert.ToInt32(Console.ReadLine());
                        try
                        {
                            if ((curse > 6) || (curse < 1))
                                throw new Exception("Curse cant be more than 6 and less than 1");
                            else k++;
                        }

                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }

                    }
                    while (k!=1);

                    Console.Write("Group: ");
                    group = Console.ReadLine();
                    Student.SetStudent(firstname, lastname, age, group, curse);
                    break;

                case "out":
                    Console.Clear();
                    Console.WriteLine("Data about student");
                    Console.WriteLine("\n"+Student.GetStudent());
                    break;

                default:
                    Console.WriteLine("Choose other mode");
                    break;

            }
            goto label1;

        }
    }
}
