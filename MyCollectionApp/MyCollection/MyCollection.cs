﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mycoll
{
    public class MyCollection<T>: IEnumerable<T>
    {
        private List<T> List = new List<T>();
       

        public T this[int i]
        {
            get {return List[i];}

            set { List[i] = value;}
        }

        public void Add(T value)
        {
            List.Add(value);
        }

        public IEnumerator<T> GetEnumerator()
        {
            return List.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }
}
