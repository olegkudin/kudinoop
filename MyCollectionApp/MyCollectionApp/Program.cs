﻿using MainManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyCollectionApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Manager manager = new Manager();
            manager.ValueGenerator();
            manager.ShowColl();
        }
    }
}
