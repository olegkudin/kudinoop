﻿using mycoll;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainManager
{
    public class Manager
    {
        MyCollection<int> coll = new MyCollection<int>();

        public void ValueGenerator()
        {
            Random rand = new Random();
            Console.WriteLine("Type length: ");
            var len = Convert.ToInt32(Console.ReadLine());
            for (int i = 0; i < len; i++)
            {
                coll.Add(rand.Next(0, 10));
            }
        }

        public void ShowColl()
        {
            Console.WriteLine($"Result:\n");
            foreach (var item in coll)
            {
                Console.Write(item + " ");
            }
            Console.ReadLine();

        }
    }
}
