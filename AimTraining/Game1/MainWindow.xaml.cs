﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Game1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
       
        OleDbConnection conn = new OleDbConnection(@"Provider = Microsoft.ACE.OLEDB.12.0; Data Source=" + System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), "RecordDB.accdb"));
        OleDbDataAdapter DA = new OleDbDataAdapter();
        OleDbCommandBuilder CB = new OleDbCommandBuilder();
        DataSet DS = new DataSet();
        System.Windows.Threading.DispatcherTimer HideTimer = new System.Windows.Threading.DispatcherTimer();
        System.Windows.Threading.DispatcherTimer ShowTimer = new System.Windows.Threading.DispatcherTimer();
        System.Windows.Threading.DispatcherTimer GameTimer = new System.Windows.Threading.DispatcherTimer();
        System.Windows.Threading.DispatcherTimer PointsTimer = new System.Windows.Threading.DispatcherTimer();
        System.Windows.Threading.DispatcherTimer PosPointsTimer = new System.Windows.Threading.DispatcherTimer();
        int time = 60;
        int hit = 0;
        int miss = 0;
        int Points = 0;
        int result_points = 0;
        Random rand = new Random();
        int left;
        int top;
        string nickname;

        RecordWindow RecordForm = new RecordWindow();
        

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            PointsTimer.Tick += new EventHandler(PointsTick);
            PointsTimer.Interval = new TimeSpan(0, 0, 0, 0, 1);

            HideTimer.Tick += new EventHandler(HideTick);
            HideTimer.Interval = new TimeSpan(0, 0, 0, 0, 500);

            ShowTimer.Tick += new EventHandler(ShowTick);
            ShowTimer.Interval = new TimeSpan(0, 0, 2);

            GameTimer.Tick += new EventHandler(GameTick);
            GameTimer.Interval = new TimeSpan(0, 0, 1);

            PosPointsTimer.Tick += new EventHandler(PosPointsTick);
            PosPointsTimer.Interval = new TimeSpan(0, 0, 1);
         
            conn.Open();
            DA = new OleDbDataAdapter("SELECT TOP 10 Nickname, Hits, Misses, Score from Records ORDER BY Score DESC ", conn);
            CB = new OleDbCommandBuilder(DA);
            DA.Fill(DS, "Records");          
        }

        private void button_target(object sender, RoutedEventArgs e)
        {
            hit++;
            target.Visibility = Visibility.Hidden;
            PointsTimer.Stop();
            result_points += Points;
            label_points.Content = Points;        
            label_points.Visibility = Visibility.Visible;
            Canvas.SetLeft(label_points, left);
            Canvas.SetTop(label_points, top);
            PosPointsTimer.Start();
            label_score.Content = "Score: " + result_points;
        }

        private void HideTick(object sender, EventArgs e)
        {
            target.Visibility = Visibility.Hidden;           
        }

        private void PosPointsTick(object sender, EventArgs e)
        {
            label_points.Visibility = Visibility.Hidden;
            PosPointsTimer.Stop();
        }

        private void ShowTick(object sender, EventArgs e)
        {
            HideTimer.Stop();
            Points = 1000;           
            Canvas.SetLeft(target, left = rand.Next(10,543));
            Canvas.SetTop(target, top = rand.Next(10, 522));
            target.Visibility = Visibility.Visible;                      
            PointsTimer.Start();            
            HideTimer.Start();
            
        }

        private void PointsTick(object sender, EventArgs e)
        {
            Points=Points-7;            
            label_score.Content = "Score: " + result_points;   
        }

        private void GameTick(object sender, EventArgs e)
        {           
            label_time.Content = Convert.ToString(--time);
            if (time == 0)
            {
                btn_back.Visibility = Visibility.Visible;
                btn_restart.Visibility = Visibility.Visible;
                HideTimer.Stop();
                ShowTimer.Stop();
                GameTimer.Stop();
                target.Visibility = Visibility.Hidden;
                time = 60;
                label_time.Content = Convert.ToString(time);
                label_result.Content = String.Format("RESULT\n\nHits: {0}\nMisses: {1}\nPoints: {2}", hit, miss, result_points);
               DA = new OleDbDataAdapter("Insert into Records" + "(Nickname, Hits, Misses, Score)" + "Values('" + nickname + "','" + hit + "','" + miss + "','" + result_points + "')", conn);
                CB = new OleDbCommandBuilder(DA);
                DA.Fill(DS, "Records");
                DS.Clear();
                DA = new OleDbDataAdapter("SELECT TOP 10 Nickname, Hits, Misses, Score from Records ORDER BY Score DESC ", conn);
                CB = new OleDbCommandBuilder(DA);
                DA.Fill(DS, "Records");
                label_result.Visibility = Visibility.Visible;
                label_score.Visibility = Visibility.Hidden;
                label_time.Visibility = Visibility.Hidden;
                hit = 0;
                miss = 0;
            }        
        }

        private void button_start(object sender, RoutedEventArgs e)
        {
            if (txt_nick.Text.Length > 1)
            {
                result_points = 0;
                label_nick.Visibility = Visibility.Hidden;
                label_score.Content = "Score: " + result_points;
                label_score.Visibility = Visibility.Visible;
                label_time.Visibility = Visibility.Visible;
                btn_back.Visibility = Visibility.Hidden;
                btn_restart.Visibility = Visibility.Hidden;
                label_result.Visibility = Visibility.Hidden;
                hit = 0;
                miss = 0;
                result_points = 0;
                ShowTimer.Start();
                label_start.Visibility = Visibility.Hidden;
                btn_start.Visibility = Visibility.Hidden;
                GameTimer.Start();
                nickname = txt_nick.Text;
                txt_nick.Visibility = Visibility.Hidden;
                btn_records.Visibility = Visibility.Hidden;
            }
            else
                MessageBox.Show("Type your nickname", "Error",  MessageBoxButton.OK, MessageBoxImage.Error);
        }           

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            miss++;
            Point xy = new Point();
            xy = e.GetPosition(this);
            result_points = result_points - 100;
            label_score.Content = "Score: " + result_points;
            label_points.Content = "miss";
            Canvas.SetLeft(label_points, xy.X);
            Canvas.SetTop(label_points, xy.Y);
            label_points.Visibility = Visibility.Visible;
            PosPointsTimer.Start();
            

        }

        private void btn_back_Click(object sender, RoutedEventArgs e)
        {
            label_start.Visibility = Visibility.Visible;
            btn_back.Visibility = Visibility.Hidden;
            btn_restart.Visibility = Visibility.Hidden;
            btn_start.Visibility = Visibility.Visible;
            label_result.Visibility = Visibility.Hidden;
            txt_nick.Visibility = Visibility.Visible;
            btn_records.Visibility = Visibility.Visible;
            
        }
    

        private void btn_records_Click(object sender, RoutedEventArgs e)
        {            
            RecordForm.Show();
            RecordForm.dataGrid.ItemsSource = DS.Tables["Records"].DefaultView;
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            RecordForm.Close();
        }
    }
}
