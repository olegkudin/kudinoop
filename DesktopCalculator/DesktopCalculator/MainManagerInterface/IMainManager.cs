﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MathInterface;

namespace MainManagerInterface
{
    public interface IMainManager
    {
        string GetResult(IMathOperation Operation, string exp);
    }
}
