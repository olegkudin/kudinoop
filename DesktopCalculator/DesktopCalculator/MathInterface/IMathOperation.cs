﻿namespace MathInterface
{
    public interface IMathOperation
    {
        double DoOperation(string exp);
    }
}
