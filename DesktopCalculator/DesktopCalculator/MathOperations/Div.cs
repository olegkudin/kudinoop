﻿using MathInterface;
using System;

namespace MathOperations
{
    public class Div: IMathOperation
    {
        public double DoOperation(string exp)
        {
            double result = 0;
            var operator1 = Convert.ToDouble(exp.Substring(0, exp.IndexOf("/")));
            var operator2 = Convert.ToDouble(exp.Substring((exp.IndexOf("/")+1), exp.Length - exp.IndexOf("/")-1));
            if (operator2 == 0)
            {
                throw new DivideByZeroException();
            }
            result = operator1 / operator2;
            return result;
        }
    }
}
