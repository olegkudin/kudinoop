﻿using System;
using MathInterface;

namespace MathOperations
{
    public class Plus : IMathOperation
    {
        public double DoOperation(string exp)
        {
            double result = 0;
            var operator1 = Convert.ToDouble(exp.Substring(0, exp.IndexOf("+")));
            var operator2 = Convert.ToDouble(exp.Substring((exp.IndexOf("+")), exp.Length - exp.IndexOf("+")));
            result = operator1 + operator2;
            return result;
        }
    }
}
