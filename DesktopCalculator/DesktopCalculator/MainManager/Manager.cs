﻿using MainManagerInterface;
using MathInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainManager
{
    public class Manager: IMainManager
    {
        public string GetResult(IMathOperation Operation, string exp)
        {
            string result = Convert.ToString(Operation.DoOperation(exp));
            return result;
        }
    }
}
