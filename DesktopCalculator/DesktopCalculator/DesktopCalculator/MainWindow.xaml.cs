﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MainManager;
using MathOperations;

namespace DesktopCalculator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        int plus = 0;
        int minus = 0;
        int multi = 0;
        int div = 0;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void button_one_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btNumber_Click(object sender, RoutedEventArgs e)
        {
            var btNumber = sender as Button;
            if (btNumber != null)
            {
                var inputDate = txtInOut.Text;
                if (inputDate.Equals("0"))
                {
                    inputDate = $"{btNumber.Tag}";
                }
                else
                {
                    inputDate = $"{inputDate}{btNumber.Tag}";
                }
                txtInOut.Text = inputDate;
            }
        }

        private void btOperation_Click(object sender, RoutedEventArgs e)
        {
            var btOperation = sender as Button;
            if (btOperation != null)
            {
                var inputDate = txtInOut.Text;
                var last = inputDate.Substring(inputDate.Length - 1, 1);               
                if ((last == "+") || (last == "-") || (last == "/") || (last == "*"))
                {
                    inputDate = inputDate.Substring(0, inputDate.Length - 1);
                    inputDate += btOperation.Content;
                }
                else
                {
                    inputDate = $"{inputDate}{btOperation.Content}";
                }
                    txtInOut.Text = inputDate;
            }
        }

        private void button_plus_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void button_clear_Click(object sender, RoutedEventArgs e)
        {
            txtInOut.Text = "0";
        }

        private void txtInOut_KeyDown(object sender, KeyEventArgs e)
        {
            int ch = Convert.ToInt32(e.Key);
                             
            if (e.Key!=Key.Back && 
                e.Key != Key.OemMinus && 
               e.Key !=  Key.OemPlus &&
                ch != 87 &&
                ch != 89 &&
                ch != 84 &&
                ch != 85 &&
                (ch < 34 || ch > 43))
                e.Handled = true;
        }

        private void button_equals_Click(object sender, RoutedEventArgs e)
        {
            var exp = txtInOut.Text;
            string result="";
            Manager manager = new Manager();
            if (exp.Contains("+") == true)
            {
               result = manager.GetResult(new Plus(), exp);
            }
            else
            if (exp.Contains("-") == true)
            {
                result = manager.GetResult(new Minus(), exp);
            }
            else
            if (exp.Contains("*") == true)
            {
                result = manager.GetResult(new Multi(), exp);                
            }
            else
            if (exp.Contains("/") == true)
            {
                try
                {
                    result = manager.GetResult(new Div(), exp);
                }
                catch (DivideByZeroException)
                {
                    result = "Divide by zero";
                }
                
            }
            txtInOut.Text += " = " + result;

        }

        private void txtInOut_GotMouseCapture(object sender, MouseEventArgs e)
        {
            txtInOut.SelectAll();
        }
    }
}
